package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test_Serie4_EntreesClavier extends SimulConsole {

    @Test
    @Order(1)
    void test_clavier1() throws Exception {
        choixMenu("4a");
        ecrire("1");
        assertSortie("1", false);
        choixMenu("4a");
        ecrire("10");
        assertSortie("10", false);
    }

    @Test
    @Order(2)
    void test_clavier2() throws Exception {
        choixMenu("4b");
        ecrire("11");
        assertSortie("11", false);
        choixMenu("4b");
        ecrire("101");
        assertSortie("101", false);
    }

    @Test
    @Order(3)
    void test_clavier3() throws Exception {
        getSortie();
        choixMenu("4c");
        ecrire("allo");
        ecrire("1");
        assertSortie("allo", false);
        assertSortie("1", false);

    }

    @Test
    @Order(4)
    void test_clavier4() throws Exception {
        choixMenu("4d");
        ecrire("1");
        ecrire("2");
        ecrire("3");

        assertSortie("3", false);
        assertSortie("2", false);
        assertSortie("1", false);

    }

    @Test
    @Order(5)
    void test_clavier5() throws Exception {
        choixMenu("4e");
        ecrire("a b c d e");
        assertSortie("a", false);
        assertSortie("b", false);
        assertSortie("c", false);
        assertSortie("d", false);
        assertSortie("e", false);

    }

    @Test
    @Order(6)
    void test_clavier6() throws Exception {
        choixMenu("4f");
        ecrire("allo");
        ecrire("toi");
        assertSortie("toi allo", false);

    }


}
